#!/usr/bin/env python
"""Generate setup.py from a template."""
import re


SETUP_TPL = """from setuptools import setup, find_packages


setup(
    name='agentk',
    version='{version}',
    url='https://gitlab.com/kubic-ci/k',
    author='Yauhen Yakimovich',
    author_email='eugeny.yakimovitch@gmail.com',
    description='"AGENT" K is a complete minimalistic kubectl "doner"-wrap',
    packages=find_packages(),
    install_requires={requirements},
    scripts=['k', 'k.bat'],
    license='MIT',
)
"""

if __name__ != "__main__":
    raise Exception("run me, don't import")


def get_version():
    """Grab version from the agent "k" script."""
    with open("k") as fp:
        for line in fp:
            if "__version__ = " in line:
                m = re.search(r'\=\s*\"([^"]*)\"', line)
                return m.group(1)
    raise ValueError("Failed to find version.")


def get_requirements():
    """Grab package requirements from the txt."""
    with open("requirements.txt") as fp:
       return [line.strip() for line in fp if line]


with open("setup.py", "w+") as output:
    vars = {
        "version": get_version(),
        "requirements": get_requirements(),
    }
    template = SETUP_TPL.format(**vars)
    output.write(template)
